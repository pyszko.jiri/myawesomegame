import { AppBar, Toolbar, Typography } from '@material-ui/core';
import React from 'react';

interface Props {}

const Header: React.FC<Props> = () => {
  return (
    <AppBar position="static">
      <Toolbar>
        <Typography variant="h6">Game</Typography>
      </Toolbar>
    </AppBar>
  );
};

export default Header;
