import React from "react";

import { Container as MDContainer } from "@material-ui/core";

interface Props {}

const Container: React.FC<Props> = () => {
  return <MDContainer>Hello world!</MDContainer>;
};

export default Container;
