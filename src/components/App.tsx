import './App.css';

import React from 'react';

import Container from './layout/container/Container';
import Header from './layout/header/Header';

const App: React.FC = () => {
  return (
    <>
      <Header />
      <Container />
    </>
  );
};

export default App;
